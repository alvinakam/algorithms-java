import graph.*;

import java.util.*;

import java.lang.*;

public class Prim {

  Graph<Integer, Integer> primGraph;
  Graph<Integer, Integer> finalGraph;


    public Prim(Graph<Integer, Integer> graph) {
        //TODO: Using the passed in graph, implement Prims algorithm in this
        // class.

        primGraph = graph;
        finalGraph = new DirectedGraph<Integer, Integer>();

    }


    public Graph<Integer, Integer> getMinimumSpanningTree() {
        //TODO: You should return a new graph that represents the minimum
        // spanning tree of the graph.

        List<Node<Integer>> allNodes =  primGraph.getNodes(); //get all nodes in the graph : public List<Node<X>> getNodes();

        for(Node<Integer> node : allNodes){
          finalGraph.add(node);
        }

        List<Node<Integer>> visitedNodes = new ArrayList<Node<Integer>>();
        List<Edge<Integer, Integer>> edges = new ArrayList<Edge<Integer, Integer>>();

        Node<Integer> startNode = primGraph.getNode(1); //Returns the node in the graph with the specified index. Returns null if there isn't a node with that index in the graph.
                //public Node<X> getNode(X index);

//        System.out.println("!!!!!!list of all nodes 3 !!!!!!!" + Arrays.toString(allNodes.toArray()));

        visitedNodes.add(startNode);
//        System.out.println("!!!!!!list of visited nodes!!!!" + Arrays.toString(visitedNodes.toArray()));
//        System.out.println("!!!!!!list of all nodes!!!!" + Arrays.toString(allNodes.toArray()));


        int numberNodesVisited = visitedNodes.size();
    //    System.out.println("!! no nodes visited !!" + numberNodesVisited);
        int numberAllNodes = allNodes.size();
    //    System.out.println("!! total nodes !!" + numberAllNodes);
        //List<Edge<Integer, Integer>> possibleEdges = graph.getEdgesFrom(startNode); //Returns all edges in the graph where the source of the edge is the specified node. Returns null if there aren't any edges.

        Node<Integer> currentNode = startNode;
//        System.out.println("!!!!!!CURRENT NODE start!!!!!!!" + currentNode.toString());

        List<Edge<Integer, Integer>> possibleEdges = primGraph.getEdgesFrom(currentNode); //Returns all edges in the graph where the source of the edge is the specified node. Returns null if there aren't any edges.
                                  //public List<Edge<X, Y>> getEdgesFrom(Node<X> node);
//        System.out.println("!!!!!!list of 1st possible edges !!!!!!!" + Arrays.toString(possibleEdges.toArray()));

        List<Edge<Integer, Integer>> currentEdges = new ArrayList<Edge<Integer, Integer>>(); //Returns all edges in the graph where the source of the edge is the specified node. Returns null if there aren't any edges.

        while(numberNodesVisited < numberAllNodes){
    //      System.out.println("!!!!!!CURRENT NODE!!!!!!!" + currentNode.toString());

          currentEdges = primGraph.getEdgesFrom(currentNode); //Returns all edges in the graph where the source of the edge is the specified node. Returns null if there aren't any edges.
    //      System.out.println("!!!!!!list of possible edges !!!!!!!" + Arrays.toString(possibleEdges.toArray()));
    //      System.out.println("!!!!!!list of current edges !!!!!!!" + Arrays.toString(currentEdges.toArray()));
    //      possibleEdges.clear();
          possibleEdges.addAll(currentEdges);

 // TODO: RUN A LOOP THROUGH VISITED NODES, GET POSSIBLE NODES WHILE ENSURING THEY DON'T POINT TO ANOTHER VISITED NODE (RESET THE EDGES EVERY LOOP)

    //      System.out.println("!!!!!!list of possible edges 2!!!!!!!" + Arrays.toString(possibleEdges.toArray()));
    /*      Set<Edge<Integer, Integer>> possibleEdgesDeleteDuplicates = new HashSet<>();
          possibleEdgesDeleteDuplicates.addAll(possibleEdges);
          possibleEdges.clear();
          possibleEdges.addAll(possibleEdgesDeleteDuplicates);*/
    //      System.out.println("!!!!!!list of possible edges 3!!!!!!!" + Arrays.toString(possibleEdges.toArray()));


          Edge<Integer, Integer> tempMin = possibleEdges.get(0);
    //      System.out.println("!!!!!!start tempMin edge!!!!!!!" + tempMin.toString());


          for(Edge<Integer, Integer> edge : possibleEdges){
                Node<Integer> source = edge.getSource(); //return node
                Node<Integer> target = edge.getTarget(); //return node
              //  System.out.println("!!!!!!list of edges in loop !!!!!!!" + Arrays.toString(edges.toArray()));
              //  System.out.println("!!!!!!list of visitedNodes in loop !!!!!!!" + Arrays.toString(visitedNodes.toArray()));


                if(visitedNodes.contains(source) && !visitedNodes.contains(target)){
                    if (edge.getData() < tempMin.getData()){
                      tempMin = edge;
    //                  System.out.println("!!!!!!edge in loop!!!!!!!" + edge.toString());
                    }

                }
          }

    //      System.out.println("!!!!!!replace tempMin edge!!!!!!!" + tempMin.toString());

          edges.add(tempMin); //edge contains the edges we want for mst
  //        System.out.println("!!!!!!list of final edges !!!!!!!" + Arrays.toString(edges.toArray()));

    //      primGraph.add(tempMin); //edge contains the edges we want for mst
          finalGraph.add(tempMin);
          Edge<Integer, Integer> swapTempMin = tempMin.swap();
    //      primGraph.add(swapTempMin);
          finalGraph.add(swapTempMin);
          edges.add(swapTempMin);

          possibleEdges.remove(tempMin);           //public void remove(Node<X> node);
          currentNode = tempMin.getTarget(); //Returns the target node of the edge. :  public Node<X> getTarget()
    //      System.out.println("!!!!!!CURRENT NODE after remove tempMin!!!!!!!" + currentNode.toString());

//TODO :: LOOK HERE !!! What is that for again? O.o Was it to prevent cycle in graph???? This looks slightly dodgy
    //      List<Edge<Integer,Integer>> removeEdgePointToCurrentNode = primGraph.getEdgesTo(currentNode);
          //public List<Edge<X, Y>> getEdgesTo(Node<X> node);
                //Returns all edges in the graph where the target of the edge is the specified node. Returns null if there aren't any edges.
      //    possibleEdges.removeAll(removeEdgePointToCurrentNode);

          visitedNodes.add(currentNode);

          numberNodesVisited = visitedNodes.size();
  //        System.out.println("!! number nodes visited in loop!!" + numberNodesVisited);
  //        System.out.println("!!!!!!list of visited nodes!!!!" + Arrays.toString(visitedNodes.toArray()));
  //        System.out.println("!!!!!!list of possible edges!!!!!!!" + Arrays.toString(possibleEdges.toArray()));
  //        System.out.println("END OF 1 LOOP");


        }
//    public void add(Node<X> node);
//    public void add(Edge<X, Y> edge);
System.out.println("!!!!!!list of final edges !!!!!!!" + Arrays.toString(edges.toArray()));

int numberEdgesInMST = edges.size();
System.out.println("Total number of edges in mst" + numberEdgesInMST);

        return finalGraph;

    }

}
